<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{

    private function getStructure()
    {
        return [
            'users' => function (Blueprint $table) {
                $table->increments('id');
                $table->string('first_name');
                $table->string('last_name');
                $table->string('email')->unique();
                $table->rememberToken();
                $table->timestamps();
            },
            'user_meta' => function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->string('source');
                $table->string('property');
                $table->string('value');
                $table->timestamps();
            },
            'quizzes' => function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->text('description');
                $table->enum('access', ['private', 'public'])->default('private');
                $table->string('icon_file');
                $table->string('icon_background');
                $table->timestamps();
            },
            'quiz_user' => function (Blueprint $table) {
                $table->increments('id');
                $table->integer('quiz_id')->unsigned();
                $table->foreign('quiz_id')->references('id')->on('quizzes');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->unique(['quiz_id', 'user_id']);
                $table->timestamp('available_at')->nullable();
                $table->timestamps();
            },
            'quiz_user_takes' => function (Blueprint $table) {
                $table->increments('id');
                $table->integer('quiz_id')->unsigned();
                $table->foreign('quiz_id')->references('id')->on('quizzes');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->unique(['quiz_id', 'user_id']);
                $table->decimal('score', 5, 4)->nullable();
                $table->timestamp('started_at')->nullable();
                $table->timestamp('finished_at')->nullable();
                $table->timestamps();
            },
            'questions' => function (Blueprint $table) {
                $table->increments('id');
                $table->integer('quiz_id')->unsigned();
                $table->foreign('quiz_id')->references('id')->on('quizzes');
                $table->string('content');
                $table->text('hint')->nullable();
                $table->enum('choice', ['single', 'multiple'])->default('single');
                $table->integer('weight');
                $table->timestamps();
            },
            'answers' => function (Blueprint $table) {
                $table->increments('id');
                $table->integer('question_id')->unsigned();
                $table->foreign('question_id')->references('id')->on('questions');
                $table->string('content');
                $table->integer('points');
                $table->integer('weight');
                $table->timestamps();
            },
            'answer_user' => function (Blueprint $table) {
                $table->increments('id');
                $table->integer('answer_id')->unsigned();
                $table->foreign('answer_id')->references('id')->on('answers');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->unique(['answer_id', 'user_id']);
                $table->timestamps();
            },
            'keys' => function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->string('key')->unique();
                $table->timestamps();
            },
            'groups' => function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->timestamps();
            },
            'group_user' => function (Blueprint $table) {
                $table->increments('id');
                $table->integer('group_id')->unsigned();
                $table->foreign('group_id')->references('id')->on('groups');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->timestamps();
            },
            'group_quiz' => function (Blueprint $table) {
                $table->increments('id');
                $table->integer('group_id')->unsigned();
                $table->foreign('group_id')->references('id')->on('groups');
                $table->integer('quiz_id')->unsigned();
                $table->foreign('quiz_id')->references('id')->on('quizzes');
                $table->timestamp('available_at')->nullable();
                $table->timestamps();
            },
            'permissions' => function (Blueprint $table) {
                $table->increments('id');
                $table->string('key')->unique();
                $table->timestamps();
            },
            'permission_user' => function (Blueprint $table) {
                $table->increments('id');
                $table->integer('permission_id')->unsigned();
                $table->foreign('permission_id')->references('id')->on('permissions');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->timestamps();
            }
        ];
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->getStructure() as $table => $callback) {
            Schema::create($table, $callback);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tables = array_keys($this->getStructure());

        // Drop tables in reversed order
        foreach (array_reverse($tables) as $table) {
            Schema::dropIfExists($table);
        }
    }
}
