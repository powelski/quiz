<?php

namespace Quiz;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function quiz()
    {
        return $this->belongsTo('Quiz\Quiz');
    }

    public function answers()
    {
        return $this->hasMany('Quiz\Answer');
    }
}
