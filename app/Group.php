<?php

namespace Quiz;

use DB;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function users()
    {
        return $this->belongsToMany('Quiz\User')->withTimestamps();
    }

    public function quizzes()
    {
        return $this
            ->belongsToMany('Quiz\Quiz')
            ->wherePivot('available_at', '<=', DB::raw('NOW()'))
            ->withTimestamps();
    }
}
