<?php

namespace Quiz\Exceptions;

use Exception;
use Quiz\Exceptions\InputException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($request->ajax()) {
            $response = [];
            $status   = 200;

            if ($e instanceof InputException) {
                $response['field'] = $e->getField();
                $response['error'] = $e->getMessage();
                $status = $e->getStatusCode();
            } elseif ($e instanceof HttpException) {
                $response['error'] = $e->getMessage();
                $status = $e->getStatusCode();
            } else {
                $response['error'] = config('app.debug') ? $e->getMessage() : 'Internal error occurred.';
            }

            return response()->json($response, $status);
        }

        return parent::render($request, $e);
    }
}
