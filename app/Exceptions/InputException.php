<?php

namespace Quiz\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class InputException extends HttpException
{
    private $statusCode;
    private $headers;
    private $field;

    public function __construct($statusCode, $field, $message, Exception $previous = null, array $headers = array(), $code = 0)
    {
        $this->field = $field;

        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }

    public function getField()
    {
        return $this->field;
    }
}
