<?php

namespace Quiz\Http\Controllers;

use Illuminate\Http\Request;

use Quiz\Http\Requests;
use Quiz\Http\Controllers\Controller;

use JavaScript;

class HomeController extends Controller
{
    public function index()
    {
        $languages = [
            'en',
            'pl'
        ];

        JavaScript::put([
            'language'  => config('app.locale'),
            'routes'    => [
                'api' => url(config('api.uri'))
            ],
            'translations' => array_combine(
                $languages,
                array_map(
                    function ($language) {
                        return trans('quiz', [], null, $language);
                    },
                    $languages
                )
            )
        ]);

        return view('home')
            ->withLanguages($languages)
            ->withPages([
                'home',
                'guest',
                'register',
                'quizzes',
                'intro',
                'quiz'
            ]);
    }
}
