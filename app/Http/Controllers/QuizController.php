<?php

namespace Quiz\Http\Controllers;

use Quiz\Key;
use Quiz\Quiz;
use Quiz\User;
use Quiz\Group;
use Quiz\Answer;
use Illuminate\Http\Request;
use Quiz\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as DB;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class QuizController extends Controller
{
    protected $db;
    protected $key;
    protected $quiz;
    protected $answer;
    protected $request;

    public function __construct(
        DB $db,
        Key $key,
        Quiz $quiz,
        Answer $answer,
        Request $request
    ) {
        $this->db       = $db;
        $this->key      = $key;
        $this->quiz     = $quiz;
        $this->answer   = $answer;
        $this->request  = $request;
    }

    private function attachData(array $keys, User $user)
    {
        $map = [
            'me' => function (User $user) {
                return [
                    'first_name' => $user->first_name,
                    'last_name'  => $user->last_name,
                    'language'   => $user->language
                ];
            },
            'quizzes' => function (User $user) {
                return $this->userQuizzes($user);
            }
        ];

        $result = [];

        $keys = array_values(array_unique($keys));

        $invalidKeys = array_diff($keys, array_keys($map));

        if (count($invalidKeys) > 0) {
            $message = 'Unknown with parameter value';

            if (count($invalidKeys) !== 1) {
                $message .= 's';
            }

            $message .= ': ' . implode(', ', $invalidKeys) . '.';

            throw new HttpException(400, $message);
        }

        foreach ($keys as $key) {
            $result[$key] = $map[$key]($user);
        }

        return $result;
    }

    private function userQuizzes(User $user)
    {
        $quizzes = $this->quiz->available($user)
            ->orderBy('created_at')
            ->get([
                'id',
                'title',
                'description',
                'icon_file',
                'icon_background'
            ])
            ->map(function ($quiz) use ($user) {
                $take = $quiz
                    ->takers()
                    ->find($user->id);

                if ($take !== null) {
                    $quiz->started_at = $take->pivot->started_at;

                    if ($take->pivot->finished_at !== null) {
                        $quiz->finished_at = $take->pivot->finished_at;

                        if ($quiz->isGradable()) {
                            if ($take->pivot->score !== null) {
                                $quiz->score = (float) $take->pivot->score;
                            }
                        }

                        $quiz->grade = $user->getGrade($quiz);
                    }
                }

                return array_filter(
                    $quiz->toArray(),
                    function ($value) {
                        return $value !== null;
                    }
                );
            });

        return $quizzes;
    }

    public function init()
    {
        $key = $this->key->whereKey($this->request->input('key'))->first();

        if ($key === null) {
            throw new AccessDeniedHttpException('Invalid API key.');
        }

        $me = $key->user()->firstOrFail();

        $response = [
            'success' => true
        ];

        if ($this->request->has('with')) {
            $response += $this->attachData(explode(',', $this->request->input('with')), $me);
        }

        return $response;
    }

    public function quiz($id)
    {
        $quiz = $this->quiz->find($id);

        if ($quiz === null) {
            throw new NotFoundHttpException('Quiz not found.');
        }

        $key = $this->key->whereKey($this->request->input('key'))->first();

        if ($key === null) {
            throw new AccessDeniedHttpException('Invalid API key.');
        }

        $me = $key->user()->firstOrFail();

        if ($this->quiz->available($me)->find($id) === null) {
            throw new AccessDeniedHttpException('Access denied.');
        }

        if ($this->request->has('answers')) {
            $answersIds = $this->request->input('answers');

            if ($answersIds === 'none') {
                $answersIds = [];
            }

            if (!is_array($answersIds)) {
                throw new HttpException(400, 'Invalid answers parameter.');
            }

            if (count($answersIds) !== count(array_unique($answersIds))) {
                throw new HttpException(400, 'Duplicate answer ID.');
            }

            $assignment = $quiz->takers()->find($me->id);

            if ($assignment === null || $assignment->pivot->started_at === null || $assignment->pivot->finished_at !== null) {
                throw new AccessDeniedHttpException('Quiz cannot be solved.');
            }

            $answers = $quiz
                ->answers()
                ->whereIn('answers.id', $answersIds)
                ->get(['answers.question_id', 'questions.choice']);

            if ($answers->count() !== count($answersIds)) {
                throw new HttpException(400, 'Invalid answer ID.');
            }

            $violatedQuestionsCount = $answers
                ->groupBy('question_id')
                ->filter(function ($answers) {
                    return ($answers[0]->choice === 'single' && count($answers) >= 2);
                })
                ->count();

            if ($violatedQuestionsCount > 0) {
                throw new HttpException(400, 'Violated question rules.');
            }

            $existingAnswersIds = $this->answer
                                       ->select('answers.id')
                                       ->join('answer_user', 'answer_user.answer_id', '=', 'answers.id')
                                       ->join('questions', 'questions.id', '=', 'answers.question_id')
                                       ->where('answer_user.user_id', $me->id)
                                       ->where('questions.quiz_id', $quiz->id)
                                       ->lists('id')
                                       ->all();

            $deleteIds = array_diff($existingAnswersIds, $answersIds);

            if (!empty($deleteIds)) {
                // checking, because detach([]) deletes everything!
                $me->answers()->detach($deleteIds);
            }

            $insertIds = array_diff($answersIds, $existingAnswersIds);

            if (!empty($insertIds)) {
                // checking, because attach([]) throws an error
                $me->answers()->attach($insertIds);
            }

            $quiz->takers()->updateExistingPivot($me->id, [
                'finished_at' => $this->db->raw('NOW()')
            ]);

            $score = $me->countScore($quiz);

            if ($score === false) {
                $score = null;
            }

            $quiz->takers()->updateExistingPivot($me->id, [
                'score' => $score
            ]);

            $response = [
                'success' => true
            ];

            if ($this->request->has('with')) {
                $response += $this->attachData(explode(',', $this->request->input('with')), $me);
            }
        } else {
            $quiz = $this->quiz
                ->with([
                    'questions' => function ($query) {
                        $query
                            ->select(
                                'id',
                                'quiz_id',
                                'content',
                                'hint',
                                'choice'
                            )
                            ->orderBy('weight')
                            ->orderBy($this->db->raw('RAND()'));
                    },
                    'questions.answers' => function ($query) use ($me) {
                        $query
                            ->select(
                                'answers.id',
                                'answers.question_id',
                                'answers.content',
                                'answers.points',
                                'answer_user.user_id'
                            )
                            ->leftJoin('answer_user', function ($join) use ($me) {
                                $join
                                    ->on('answer_user.answer_id', '=', 'answers.id')
                                    ->on('answer_user.user_id', '=', $this->db->raw($me->id));
                            })
                            ->orderBy('answers.weight')
                            ->orderBy($this->db->raw('RAND()'));
                    }
                ])
                ->findOrFail($quiz->id, [
                    'id',
                    'title',
                    'description',
                    'icon_file',
                    'icon_background'
                ]);

            $assignment = $quiz->takers()->find($me->id);

            if ($assignment === null || $assignment->pivot->started_at === null) {
                $pivotUpdates = [
                    'started_at' => $this->db->raw('NOW()')
                ];

                if ($assignment === null) {
                    $quiz->takers()->save($me, $pivotUpdates);
                } else {
                    $quiz->takers()->updateExistingPivot($me->id, $pivotUpdates);
                }

                $assignment = $quiz->takers()->find($me->id);
            }

            $quiz = $quiz->toArray() + $assignment->pivot->toArray();

            $quiz = array_filter(
                $quiz,
                function ($value) {
                    return $value !== null;
                }
            );

            $quiz['questions'] = array_map(
                function ($question) use ($assignment) {
                    unset(
                        $question['id'],
                        $question['quiz_id']
                    );

                    $question = array_filter(
                        $question,
                        function ($value) {
                            return $value !== null;
                        }
                    );

                    $question['answers'] = array_map(
                        function ($answer) use ($question, $assignment) {
                            if ($assignment->pivot->finished_at !== null) {
                                if ($answer['points'] > 0) {
                                    $answer['status'] = 'correct';
                                } elseif ($answer['points'] < 0) {
                                    $answer['status'] = 'wrong';
                                } else {
                                    $answer['status'] = 'neutral';
                                }
                                $answer['selected'] = ($answer['user_id'] !== null);
                                unset($answer['user_id']);
                            }

                            unset(
                                $answer['question_id'],
                                $answer['user_id'],
                                $answer['points']
                            );

                            return $answer;
                        },
                        $question['answers']
                    );

                    return $question;
                },
                $quiz['questions']
            );

            $response = $quiz;
        }

        return $response;
    }
}
