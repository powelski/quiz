<?php

namespace Quiz\Http\Controllers;

use Quiz\Key;
use Quiz\User;
use Quiz\UserMeta;
use Facebook\Facebook;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Quiz\Exceptions\InputException;
use Quiz\Http\Controllers\Controller;
use Illuminate\Auth\AuthManager as Auth;
use Illuminate\Contracts\Hashing\Hasher as Hash;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AuthenticationController extends Controller
{
    protected $key;
    protected $str;
    protected $auth;
    protected $hash;
    protected $user;
    protected $request;
    protected $userMeta;

    public function __construct(
        Key $key,
        Str $str,
        Auth $auth,
        Hash $hash,
        User $user,
        Request $request,
        UserMeta $userMeta
    ) {
        $this->key      = $key;
        $this->str      = $str;
        $this->auth     = $auth;
        $this->hash     = $hash;
        $this->user     = $user;
        $this->request  = $request;
        $this->userMeta = $userMeta;
    }

    public function login()
    {
        $me = $this->user->whereEmail($this->request->input('email'))->first();

        if ($me === null) {
            throw new InputException(400, 'email', 'unexisting_email');
        }

        if (!$this->auth->validate($this->request->only('email', 'password'))) {
            throw new InputException(403, 'password', 'login_failed');
        }

        do {
            $random = $this->str->lower(str_random(32));
        } while ($this->key->whereKey($random)->count() > 0);

        $key = $this->key->newInstance();
        $key->user()->associate($me);
        $key->key = $random;
        $key->save();

        $response = [];

        if ($this->request->has('with')) {
            $response += $this->attachData(explode(',', $this->request->input('with')), $me);
        }

        $response['key'] = $key->key;

        return $response;
    }

    public function facebookLogin(Request $request)
    {
        $facebook = new Facebook([
            'app_id'                => env('FACEBOOK_APP_ID'),
            'app_secret'            => env('FACEBOOK_APP_SECRET'),
            'default_access_token'  => $request->input('accessToken'),
            'default_graph_version' => 'v2.4'
        ]);

        $fields = [
            'id',
            'name',
            'first_name',
            'middle_name',
            'last_name',
            'email',
            'age_range',
            'link',
            'gender',
            'locale',
            'timezone',
            'updated_time',
            'verified'
        ];

        $facebookMe = $facebook->get('/me?fields=' . implode(',', $fields))
                               ->getGraphUser()
                               ->asArray();

        $facebookMe = (object) $facebookMe;

        $userId = $this->userMeta
                       ->whereSource('Facebook')
                       ->whereProperty('id')
                       ->whereValue(json_encode($facebookMe->id, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
                       ->orderBy('created_at', 'desc')
                       ->value('user_id');

        if ($userId !== null) {
            $me = $this->user->find($userId);

            $myFacebookMeta = $me->meta()
                                 ->whereSource('Facebook')
                                 ->orderBy('created_at', 'desc')
                                 ->get()
                                 ->groupBy('property')
                                 ->map(function ($entry) {
                                     return $entry->first()->value;
                                 });
        } else {
            $me = $this->user
                       ->whereEmail($facebookMe->email)
                       ->first();
        }

        if ($me === null) {
            $me = $this->user->newInstance();
        }

        switch ($facebookMe->locale) {
            case "pl_PL":
                $language = 'pl';
                break;
            default:
                $language = 'en';
        }

        $me->email      = $facebookMe->email;
        $me->first_name = $facebookMe->first_name;
        $me->last_name  = $facebookMe->last_name;
        $me->language   = $language;
        $me->save();

        foreach ($facebookMe as $property => $value) {
            if (isset($myFacebookMeta) && json_encode($myFacebookMeta->get($property)) === json_encode($value)) {
                continue;
            }

            $meta = $this->userMeta->newInstance();
            $meta->user()->associate($me);
            $meta->source   = 'Facebook';
            $meta->property = $property;
            $meta->value    = $value;
            $meta->save();
        }

        $response = [];

        if ($request->has('with')) {
            $response += $this->attachData(explode(',', $request->input('with')), $me);
        }

        return $response;
    }

    public function logout()
    {
        $key = $this->key->whereKey($this->request->input('key'))->first();

        if ($key === null) {
            throw new AccessDeniedHttpException('Invalid API key.');
        }

        $key->delete();

        return [
            'success' => true
        ];
    }

    public function register()
    {
        extract($this->request->only('email', 'password', 'firstName', 'lastName'));

        if ($this->user->whereEmail($email)->count() > 0) {
            throw new InputException(400, 'email', 'E-mail is already registered.');
        }

        if ($this->str->length($password) < 7) {
            throw new InputException(400, 'password', 'Password must be at least 7 characters long.');
        }

        $me = $this->user->newInstance();
        $me->email      = $email;
        $me->password   = $this->hash->make($password);
        $me->first_name = $firstName;
        $me->last_name  = $lastName;
        $me->save();

        do {
            $random = $this->str->lower(str_random(32));
        } while ($this->key->whereKey($random)->count() > 0);

        $key = $this->key->newInstance();
        $key->user()->associate($me);
        $key->key = $random;
        $key->save();

        $response = [];

        if ($this->request->has('with')) {
            $response += $this->attachData(explode(',', $this->request->input('with')), $me);
        }

        $response['key'] = $key->key;

        return $response;
    }
}
