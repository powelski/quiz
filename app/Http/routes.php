<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

get('/', 'HomeController@index');

Route::group(['prefix' => config('api.uri'), 'middleware' => 'json'], function () {
    get('init', ['uses' => 'QuizController@init', 'middleware' => 'require:key']);
    post('quiz/{id}', ['uses' => 'QuizController@quiz', 'middleware' => 'require:key']);

    post('login', ['uses' => 'AuthenticationController@login', 'middleware' => 'require:email,password']);
    post('login/facebook', ['uses' => 'AuthenticationController@facebookLogin', 'middleware' => 'require:accessToken']);
    post('logout', ['uses' => 'AuthenticationController@logout', 'middleware' => 'require:key']);
    post('register', ['uses' => 'AuthenticationController@register', 'middleware' => 'require:email,password,firstName,lastName']);
});
