<?php

namespace Quiz\Http\Middleware;

use Closure;

class Json
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $content = $response->getOriginalContent();

        if ($content instanceof Jsonable) {
            $content = $content->toJson();
        } else {
            $content = json_encode($content);
        }

        return $response->header('Content-Type', 'application/json')->setContent($content);
    }
}
