<?php

namespace Quiz\Http\Middleware;

use Closure;
use Quiz\Exceptions\InputException;

class RequireInput
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$fields)
    {
        foreach ($fields as $field) {
            if (!$request->has($field)) {
                throw new InputException(400, $field, 'Missing ' . $field . ' parameter.');
            }
        }

        return $next($request);
    }
}
