<?php

namespace Quiz;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    public function user()
    {
        return $this->belongsTo('Quiz\User');
    }
}
