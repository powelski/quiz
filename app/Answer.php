<?php

namespace Quiz;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public function question()
    {
        return $this->belongsTo('Quiz\Question');
    }

    public function users()
    {
        return $this->belongsToMany('Quiz\User')->withTimestamps();
    }
}
