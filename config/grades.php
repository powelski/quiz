<?php

return [

    'thresholds' => [

        'a'      => 1,
        'b'      => 0.91,
        'c_plus' => 0.81,
        'c'      => 0.71,
        'd_plus' => 0.61,
        'd'      => 0.51,
        'f'      => 0

    ]

];
