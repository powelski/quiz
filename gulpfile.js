var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass("quiz.scss", "public/css/quiz.css")
       .scripts([
           "bower_components/jQuery/dist/jquery.min.js",
           "bower_components/moment/min/moment.min.js",
           "bower_components/moment/locale/pl.js",
           "bower_components/fastclick/lib/fastclick.js",
           "Config.js",
           "Api.js",
           "Translator.js",
           "Facebook.js",
           "quiz.js"
       ], "public/js/quiz.js");
});
