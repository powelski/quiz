<?php

return [

    'got_account'              => 'I got account',
    'email_address'            => 'E-mail address',
    'password'                 => 'Password',
    'log_me_in'                => 'Log me in',
    'i_wanna_join'             => 'I wanna join',
    'register_me'              => 'Register me',
    'registration'             => 'Registration',
    'first_name'               => 'First name',
    'last_name'                => 'Last name',
    'enroll_me'                => 'Enroll me',
    'my_tests'                 => 'My tests',
    'take_test'                => 'Take test',
    'my_answers'               => 'My answers',
    'solved_at'                => 'Test was solved',
    'with_score'               => 'with score',
    'go_back'                  => 'Go back',
    'please_wait'              => 'Please wait...',
    'assessing_test'           => 'Oceniam test...',
    'finish_test'              => 'Finish test',
    'previous'                 => 'Previous',
    'next'                     => 'Next',
    'multiple_choice_question' => 'This is a multiple-choice question.',

    'grades' => [

        'passed' => 'passed',
        'below'  => 'f!',
        'a'      => 'a',
        'b'      => 'b',
        'c_plus' => 'c+',
        'c'      => 'c',
        'd_plus' => 'd+',
        'd'      => 'd',
        'f'      => 'f'

    ]

];
