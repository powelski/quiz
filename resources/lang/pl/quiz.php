<?php

return [

    'got_account'              => 'Mam już konto',
    'email_address'            => 'Adres e-mail',
    'password'                 => 'Hasło',
    'log_me_in'                => 'Zaloguj mnie',
    'i_wanna_join'             => 'Chcę się przyłączyć',
    'register_me'              => 'Zarejestruj mnie',
    'registration'             => 'Rejestracja',
    'first_name'               => 'Imię',
    'last_name'                => 'Nazwisko',
    'enroll_me'                => 'Zapisz mnie',
    'my_tests'                 => 'Moje testy',
    'take_test'                => 'Rozpocznij test',
    'my_answers'               => 'Moje odpowiedzi',
    'solved_at'                => 'Test został rozwiązany',
    'with_score'               => 'z wynikiem',
    'go_back'                  => 'Powrót',
    'please_wait'              => 'Zaczekaj chwilę...',
    'assessing_test'           => 'I\'m assessing the test...',
    'finish_test'              => 'Zakończ test',
    'previous'                 => 'Poprzednie',
    'next'                     => 'Następne',
    'multiple_choice_question' => 'To jest pytanie wielokrotnego wyboru.',

    'grades' => [

        'passed' => 'zal',
        'below'  => 'ndst!',
        'a'      => 'cel',
        'b'      => 'bdb',
        'c_plus' => 'db+',
        'c'      => 'db',
        'd_plus' => 'dst+',
        'd'      => 'dst',
        'f'      => 'ndst'

    ]

];
