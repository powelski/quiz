<div class="view left-top" data-view="guest">
	<div class="full-wrapper">
		<div class="full-content">
			<div class="authentication-forms">
				<div class="log-in-form">
					<header class="main-header" data-i18n="got_account"></header>
					<input type="email" class="email"  data-i18n-placeholder="email_address">
					<input type="password" class="password" data-i18n-placeholder="password">
					<a class="log-in button button-success" data-i18n="log_me_in"></a>
				</div>
				<div class="register-block">
					<header class="main-header" data-i18n="i_wanna_join"></header>
					<a class="register button" data-i18n="register_me"></a>
				</div>
			</div>
		</div>
	</div>
</div>
