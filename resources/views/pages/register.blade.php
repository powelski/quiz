<div class="view left-top" data-view="register">
	<div class="full-wrapper">
		<div class="full-content">
			<div class="register-form">
				<header class="main-header" data-i18n="registration"></header>
				<input type="email" class="email" data-i18n-placeholder="email_address">
				<input type="password" class="password" data-i18n-placeholder="password">
				<input type="text" class="first-name" data-i18n-placeholder="first_name">
				<input type="text" class="last-name" data-i18n-placeholder="last_name">
				<a class="register button button-success" data-i18n="enroll_me"></a>
			</div>
		</div>
	</div>
	<footer>
		<a class="back button" data-i18n="go_back"></a>
	</footer>
</div>
