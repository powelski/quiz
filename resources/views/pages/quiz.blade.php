<div class="view left-top" data-view="quiz">
	<div class="full-wrapper">
		<div class="full-content">
			<div class="loading-questions message-loading" data-i18n="please_wait"></div>
			<div class="sending-answers message-loading" data-i18n="assessing_test"></div>
		</div>
	</div>
	<header class="quiz-header">
		<div class="quiz-icon"></div>
		<div class="quiz-info">
			<div class="title"></div>
			<p class="description"></p>
		</div>
	</header>
	<div class="questions relative"></div>
	<footer>
		<div class="left">
			<a class="back button" data-i18n="go_back"></a>
		</div>
		<div class="right">
			<a class="finish button button-success fading" data-i18n="finish_test"></a>
			<a class="previous button" data-i18n="previous"></a>
			<a class="next button" data-i18n="next"></a>
		</div>
	</footer>
</div>
