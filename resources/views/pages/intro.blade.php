<div class="view left-top table" data-view="intro">
	<div class="full-wrapper">
		<div class="intro-content full-content">
			<div class="quiz-icon"></div>
			<div class="title"></div>
			<p class="description"></p>
			<a class="take-quiz button button-success" data-i18n="take_test"></a>
			<div class="quiz-results">
				<div class="quiz-results-time">
					<span data-i18n="solved_at"></span> <time class="quiz-finished-at"></time>
				</div>
				<div class="quiz-results-score">
					<span data-i18n="with_score"></span> <div class="quiz-score"></div>
				</div>
			</div>
			<a class="see-results button" data-i18n="my_answers"></a>
		</div>
	</div>
	<footer>
		<a class="back button" data-i18n="go_back"></a>
	</footer>
</div>
