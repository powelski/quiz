@extends('layouts.screen')

@section('content')

	@foreach ($pages as $page)
		@include('pages.' . $page)
	@endforeach

	<div class="me">
		<div class="my-first-name"></div>
		<div class="my-last-name"></div>
		<nav class="languages">
			@foreach ($languages as $language)<a class="language-link" data-language="{{ $language }}">{{ $language }}</a>@endforeach
		</nav>
	</div>
	<a class="log-out"></a>

@endsection

@section('scripts')
	{!! Html::script('js/quiz.js') !!}
@endsection
