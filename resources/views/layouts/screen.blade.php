<!DOCTYPE HTML>
<html lang="pl">
	<head>
		<meta charset="UTF-8">
		<title>Przetestuj się!</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		{!! Html::style('http://fonts.googleapis.com/css?family=Titillium+Web:400,400italic|Shadows+Into+Light+Two') !!}
		{!! Html::style('css/quiz.css') !!}
	</head>
	<body>
		<div class="content">
			<div class="views relative box">
				@yield('content')
			</div>
		</div>
		<div id="fb-root"></div>
		@include('laracasts-utilities')
		@yield('scripts')
	</body>
</html>
