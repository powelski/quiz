/*jslint browser: true*/
(function (init) {
    "use strict";

    init(window, window.jQuery);
}(function (window, $) {
    "use strict";

    window.Api = {
        init: function (url) {
            this.url = url.replace(/\/+$/g, "");
        },

        call: function (uri, method, data, success, error, complete) {
            if (method === undefined) {
                method = "GET";
            }

            $.ajax({
                complete: complete,
                data:     data,
                dataType: "json",
                error:    error,
                headers:  {
                    "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content")
                },
                method:   method,
                success:  success,
                url:      this.url + "/" + uri
            });
        }
    };
}));
