/*jslint browser: true*/
(function (init) {
    "use strict";

    init(window);
}(function (window) {
    "use strict";

    window.Config = {
        transition: {
            in: {
                opacity: 1
            },
            out: {
                opacity: 0
            },
            duration: 200
        }
    };
}));
