/*jslint browser: true*/
(function (init) {
    "use strict";

    init(window, window.jQuery);
}(function (window, $) {
    "use strict";

    window.Translator = {
        language: "",

        translations: [],

        $links: {},

        init: function (language, translations, $links) {
            var self = this;

            self.language     = language;
            self.translations = translations;
            self.$links       = $links;

            $.fn.i18nText = function (key) {
                this.data("i18n", key)
                    .text(self.get(key));

                return this;
            };
        },

        get: function (key) {
            var breadcrumps = [],
                depth = 0,
                breadcrump = "",
                result,
                i = 0;

            breadcrumps = key.split(".");
            depth       = breadcrumps.length;
            result      = this.translations[this.language];

            for (i = 0; i < depth; i += 1) {
                breadcrump = breadcrumps[i];

                if (result.hasOwnProperty(breadcrump)) {
                    result = result[breadcrump];
                } else {
                    return;
                }
            }

            return result;
        },

        translate: function () {
            var self = this;

            this.$links
                .removeClass("active")
                .filter(function () {
                    return $(this).data("language") === self.language;
                })
                .addClass("active");

            $("body *").each(function () {
                var key = "",
                    value = "";

                for (key in $(this).data()) {
                    if (key.indexOf("i18n") === 0) {
                        value = self.get($(this).data(key));
                        key   = key.slice(4).toLowerCase();

                        if (key === "") {
                            $(this).text(value);
                        } else {
                            $(this).attr(key, value);
                        }
                    }
                }
            });
        }
    };
}));
