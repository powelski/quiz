/*jslint browser: true*/
(function (init) {
    "use strict";

    init(
        window,
        window.jQuery
    );
}(function (
    window,
    $
) {
    "use strict";

    function statusChangeCallback(response) {
        switch (response.status) {
        case "connected":
            console.log("Logged into your app and Facebook.");
            break;
        case "not_authorized":
            console.log("The person is logged into Facebook, but not your app.");
            break;
        default:
            // response.status === "unknown"
            console.log("The person is not logged into Facebook, so we're not sure if they are logged into this app or not.");
        }

        console.log(response);
    }

    window.fbAsyncInit = function () {
        this.FB.init({
            appId:   '1673287349568652',
            xfbml:   false,
            version: 'v2.4'
        });

        this.FB.getLoginStatus(statusChangeCallback);
    };

    $("<script></script>")
        .attr({
            id:  "facebook-jssdk",
            src: "//connect.facebook.net/pl_PL/sdk.js"
        })
        .insertBefore("script:first");
}));
